﻿using Streets;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StreetsWindowsForms
{
    public partial class Form1 : Form
    {
        private StreetInfoParser _parser;
        private bool _valid;
        public Form1()
        {
            InitializeComponent();
        }

        private void cmdOpenFile_Click(object sender, EventArgs e)
        {
            openFileDialog1.Filter = "Street Definition Files|*.txt";
            if(openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                lblFile.Text = new FileInfo(openFileDialog1.FileName).Name;
                StreetInfoFileReader reader = new StreetInfoFileReader(openFileDialog1.FileName);
                reader.ReadStreetDataFomFile();
                _parser = new StreetInfoParser(reader.GetNumberingData());
                _valid = _parser.CheckIsValidNumbering();
                if (_valid)
                {
                    _parser.CalculateNumberingForStreet();
                }
                FillResult();
            }
        }

        private void FillResult()
        {
            
            lblValid.Text = _valid.ToString();
            if (_valid)
            {
                lblTotal.Text = $"Total number of houses: {_parser.TotalNumberOfHouses()}";
                lblNorth.Text = $"Number of North houses: {_parser.TotalNumberOfHouses(StreetSide.North)}";
                lblSouth.Text = $"Number of South houses: {_parser.TotalNumberOfHouses(StreetSide.South)}";
            }
            else
            {
                lblTotal.Text = string.Empty;
                lblNorth.Text = string.Empty;
                lblSouth.Text = string.Empty;
            }
            
        }
    }
}
