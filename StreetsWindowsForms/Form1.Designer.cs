﻿namespace StreetsWindowsForms
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.cmdOpenFile = new System.Windows.Forms.Button();
            this.lblFile = new System.Windows.Forms.Label();
            this.lblValid = new System.Windows.Forms.Label();
            this.lblTotal = new System.Windows.Forms.Label();
            this.lblNorth = new System.Windows.Forms.Label();
            this.lblSouth = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // cmdOpenFile
            // 
            this.cmdOpenFile.Location = new System.Drawing.Point(13, 13);
            this.cmdOpenFile.Name = "cmdOpenFile";
            this.cmdOpenFile.Size = new System.Drawing.Size(220, 59);
            this.cmdOpenFile.TabIndex = 0;
            this.cmdOpenFile.Text = "Open Street Def. File";
            this.cmdOpenFile.UseVisualStyleBackColor = true;
            this.cmdOpenFile.Click += new System.EventHandler(this.cmdOpenFile_Click);
            // 
            // lblFile
            // 
            this.lblFile.Location = new System.Drawing.Point(240, 31);
            this.lblFile.Name = "lblFile";
            this.lblFile.Size = new System.Drawing.Size(308, 29);
            this.lblFile.TabIndex = 2;
            // 
            // lblValid
            // 
            this.lblValid.Location = new System.Drawing.Point(12, 97);
            this.lblValid.Name = "lblValid";
            this.lblValid.Size = new System.Drawing.Size(287, 24);
            this.lblValid.TabIndex = 3;
            this.lblValid.Text = "<>>>>>>>>>>>>>";
            // 
            // lblTotal
            // 
            this.lblTotal.Location = new System.Drawing.Point(12, 143);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(287, 24);
            this.lblTotal.TabIndex = 4;
            this.lblTotal.Text = "<>>>>>>>>>>>>>";
            // 
            // lblNorth
            // 
            this.lblNorth.Location = new System.Drawing.Point(12, 189);
            this.lblNorth.Name = "lblNorth";
            this.lblNorth.Size = new System.Drawing.Size(287, 24);
            this.lblNorth.TabIndex = 5;
            this.lblNorth.Text = "<>>>>>>>>>>>>>";
            // 
            // lblSouth
            // 
            this.lblSouth.Location = new System.Drawing.Point(12, 235);
            this.lblSouth.Name = "lblSouth";
            this.lblSouth.Size = new System.Drawing.Size(287, 24);
            this.lblSouth.TabIndex = 6;
            this.lblSouth.Text = "<>>>>>>>>>>>>>";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(573, 449);
            this.Controls.Add(this.lblSouth);
            this.Controls.Add(this.lblNorth);
            this.Controls.Add(this.lblTotal);
            this.Controls.Add(this.lblValid);
            this.Controls.Add(this.lblFile);
            this.Controls.Add(this.cmdOpenFile);
            this.Name = "Form1";
            this.Text = "Street Planner App";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button cmdOpenFile;
        private System.Windows.Forms.Label lblFile;
        private System.Windows.Forms.Label lblValid;
        private System.Windows.Forms.Label lblTotal;
        private System.Windows.Forms.Label lblNorth;
        private System.Windows.Forms.Label lblSouth;
    }
}

