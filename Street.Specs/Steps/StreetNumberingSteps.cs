﻿using Moq;
using NUnit.Framework;
using Streets;
using System;
using TechTalk.SpecFlow;

namespace Street.Specs.Steps
{
    [Binding]
    public class StreetNumberingSteps
    {
        [Given(@"A file name which is not empty")]
        public void GivenAFileNameWhichIsNotEmpty()
        {
            Mock<IStreetInfoFileReader> reader = new Mock<IStreetInfoFileReader>();
            reader.Setup(x => x.GetNumberingData()).Returns("1 2 4 3 6 5 7 8 9 10 12 11 13 15");

            //Retrieve the contents of the file
            var numberingData = reader.Object.GetNumberingData();
            ScenarioContext.Current["numberingData"] = numberingData;
        }
        
        [Given(@"file contains a valid specification numbering scheme")]
        public void GivenFileContainsAValidSpecificationNumberingScheme()
        {
            var contents = ScenarioContext.Current.Get<string>("numberingData");
            StreetInfoParser parser = new StreetInfoParser(contents);
            ScenarioContext.Current["scenarioParser"] = parser;
            Assert.True(parser.CheckIsValidNumbering());
        }
        
        [When(@"I press on Calculate")]
        public void WhenIPressOnCalculate()
        {
            var parser = ScenarioContext.Current.Get<StreetInfoParser>("scenarioParser");
            Assert.True(parser.CalculateNumberingForStreet());
        }

        [Then(@"I like to know how many houses there are in a given street")]
        public void ThenILikeToKnowHowManyHousesThereAreInAGivenStreet()
        {
            var parser = ScenarioContext.Current.Get<StreetInfoParser>("scenarioParser");
            Assert.AreEqual(14, parser.TotalNumberOfHouses());
        }
        
        [Then(@"How many houses are on the (.*) hand side of the street")]
        public void ThenHowManyHousesAreOnTheSideOfTheStreet(StreetSide side)
        {
            var parser = ScenarioContext.Current.Get<StreetInfoParser>("scenarioParser");
            int numberHouses = side == StreetSide.North ? 8 : 6;
            Assert.AreEqual(numberHouses, parser.TotalNumberOfHouses(side));
        }
    }
}
