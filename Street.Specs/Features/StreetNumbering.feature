﻿Feature: StreetNumbering
	A town planner is responsible 
	for keeping track of the layout 
	of a street and numbering houses

Scenario: Check number of houses in street
	Given A file name which is not empty
	And file contains a valid specification numbering scheme
	When I press on Calculate
	Then I like to know how many houses there are in a given street 
	And How many houses are on the North hand side of the street
	And How many houses are on the South hand side of the street 
#this might have been separated in 3 scenarios 1. Total 2. North 3. South, but made just 1 for brevity
