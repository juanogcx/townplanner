﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Street.Specs
{
    public class DataInput
    {
        public string StreetData { get; set; }
        public int NumberNorthHouses { get; set; }
        public int NumberSouthHouses { get; set; }
    }
}
