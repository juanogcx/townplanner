﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Streets
{
    public class StreetInfoParser
    {
        private string _numberingData;
        private List<int> _northHouses;
        private List<int> _southHouses;
        public StreetInfoParser(string NumberingData)
        {
            _numberingData = NumberingData;
        }

        public int TotalNumberOfHouses()
        {
            return _southHouses.Count + _northHouses.Count;
        }

        public int TotalNumberOfHouses(StreetSide side)
        {
            return side == StreetSide.North ? _northHouses.Count : _southHouses.Count;
        }

        private bool CheckStringIntegrity()
        {
            if (string.IsNullOrEmpty(_numberingData))
                return false;
            //check if string only contains numbers or spaces
            if (!Regex.Match(_numberingData.Trim(), "[0-9 ]+").Success)
                return false;

            if (!_numberingData.Trim().Contains("1 "))
                return false;

            return true;
        }

        public bool CheckIsValidNumbering()
        {
            if (!CheckStringIntegrity())
                return false;
            
            //first make this as string[] to clean this any extra spaces between numbers
            var stringNumbers = _numberingData.Split(' ');

            List<int> northHouses = new List<int>();
            List<int> southHouses = new List<int>();

            //begin building north and south lists
            foreach (var item in stringNumbers)
            {
                string stringNumber = item.Trim();
                if (string.IsNullOrEmpty(stringNumber))
                    continue;
                int number = int.Parse(stringNumber);
                if (number % 2 == 0)
                {
                    southHouses.Add(number);
                }
                else
                {
                    northHouses.Add(number);
                }
            }

            southHouses.Sort();
            northHouses.Sort();

            if (!IsSequential(southHouses))
                return false;

            if (!IsSequential(northHouses))
                return false;

            //all success, assign lists to class members
            _southHouses = southHouses;
            _northHouses = northHouses;

            return true;
        }

        //code stolen from internet but modified to work with even and odds :)
        private bool IsSequential(List<int> array)
        {
            return array.Zip(array.Skip(1), (a, b) => (a + 2) == b).All(x => x);
        }

        public bool CalculateNumberingForStreet()
        {
            //actually, on the validation method all processing needs to occur to avoid re-processing later
            //I'm leaving this method intentionally to demonstrate any kind of extra processing in real life

            Console.WriteLine("South houses: ");
            _southHouses.ForEach(p => Console.Write($"{p} "));
            Console.WriteLine("North houses: ");
            _northHouses.ForEach(p => Console.Write($"{p} "));

            return true;
        }
    }
}
