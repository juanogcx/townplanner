﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Streets
{
    /// <summary>
    /// I'm doing this class on purpose as "File Reader' assuming
    /// that File reading is the only way to retreive content for this App 
    /// For the tests I will use Mocking to fake the reading
    /// </summary>
    public interface IStreetInfoFileReader
    {
        void ReadStreetDataFomFile();
        string GetNumberingData();
    }
    public class StreetInfoFileReader: IStreetInfoFileReader
    {
        private string _filePath;
        private string _numberingData;
        public StreetInfoFileReader(string FilePath)
        {
            _filePath = FilePath;
        }
        public void ReadStreetDataFomFile()
        {
            using (StreamReader sr = new StreamReader(_filePath))
            {
                // Read the stream to a string, and write the string to the console.
                _numberingData = sr.ReadToEnd();
            }
        }
        public string GetNumberingData() { return _numberingData; }
    }
}
